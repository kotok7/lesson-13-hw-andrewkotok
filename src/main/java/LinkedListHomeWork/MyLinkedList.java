package LinkedListHomeWork;

import ArrayListHomeWork.MyArrayList;

import java.util.*;

public class MyLinkedList<T> implements List<T> {
    private static class Node<T> {
        T value;
        Node<T> next;
        Node<T> prev;

        public Node(T value) {
            this.value = value;
        }

    }

    private Node<T> head;
    private Node<T> tail;
    private int size;


    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        boolean modified = false;
        Iterator<? extends T> e = c.iterator();
        while (e.hasNext()) {
            add(index++, e.next());
            modified = true;
        }
        return modified;
    }
//addAll done

    @Override
    public void add(int index, T element) {
        validateIndex(index);
        Node<T> newNode = new Node<T>(element);
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else if (index == 0) {
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
        } else if (index == size) {
            newNode.prev = tail;
            tail.next = newNode;
            tail = newNode;
        } else {
            Node<T> nodeRef = head;
            for (int i = 1; i < index; i++)
                nodeRef = nodeRef.next;
            newNode.next = nodeRef.next;
            nodeRef.next = newNode;
            newNode.prev = nodeRef;
            newNode.next.prev = newNode;
        }
        size++;
    }
//    add by index and value done


    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T elem : c) {
            add(elem);
        }
        return true;
    }
//    addAll done

    @Override
    public int lastIndexOf(Object o) {
        int i = this.size();
        int k = -1;
        for (T t : this) {
            if (Objects.equals(t, o)) {
                k = i;
                break;
            }
            i--;
        }
        return k;
    }
//lastIndexOff done

    @Override
    public int indexOf(Object o) {
        int i = 0;
        int k = -1;
        for (T t : this) {
            if (Objects.equals(t, o)) {
                k = i;
                break;
            }
            i++;
        }
        return k;
    }
//    indexOf done

    @Override
    public void clear() {
        for (Node<T> node = head; node != null; ) {
            Node<T> next = node.next;
            node.value = null;
            node.next = null;
            node.prev = null;
            node = next;
        }
        head = tail = null;
        size++;
    }

    //    clear() done
    @Override
    public boolean remove(Object o) {
        if (o.equals(null)) return false;
        int i = 0;
        for (T t : this) {
            if (Objects.equals(t, o)) {
                remove(i);
                return true;
            }
            i++;
        }
        return false;
    }
    // remove done

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (T t : this) {
            if (Objects.equals(t, o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] result = Arrays.copyOf(a, size);
        int i = 0;
        for (T elem : this) {
            result[i++] = (T1) elem;
        }
        return result;
    }

    @Override
    public boolean add(T elem) {
        Node<T> node = new Node<>(elem);
        if (isEmpty()) {
            head = tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        removeIf(c::contains);
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        removeIf(t -> !c.contains(t));
        return false;
    }

    @Override
    public T get(int index) {
        Node<T> node = getNode(index);
        return node.value;
    }

    @Override
    public T set(int index, T element) {
        Node<T> node = getNode(index);
        node.value = element;
        return element;
    }

    @Override
    public T remove(int index) {
        Node<T> node = getNode(index);
        removeNode(node);
        return node.value;
    }

    @Override
    public ListIterator<T> listIterator() {
//        Not to do
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        //        Not to do
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        validateIndex(toIndex);
        MyLinkedList<T> list = new MyLinkedList<>();
        Node<T> cur = getNode(fromIndex);
        for (int i = fromIndex; i < toIndex; i++) {
            list.add(cur.value);
            cur = cur.next;
        }
        return list;
    }

    private void validateIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
    }

    private class MyLinkedListIterator implements Iterator {
        private Node<T> cur;

        public MyLinkedListIterator() {
            cur = new Node<>(null);
            cur.next = head;
        }

        @Override
        public boolean hasNext() {
            return cur.next != null;
        }

        @Override
        public Object next() {
            cur = cur.next;
            T value = cur.value;
            return value;
        }

        @Override
        public void remove() {
            removeNode(cur);
        }
    }

    private void removeNode(Node<T> cur) {
        if (cur.prev == null) {
            head = cur.next;
        } else {
            cur.prev.next = cur.next;
        }
        if (cur.next == null) {
            tail = cur.prev;
        } else {
            cur.next.prev = cur.prev;
        }
        size--;
    }

    private Node<T> getNode(int index) {

        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
        Node<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

}
