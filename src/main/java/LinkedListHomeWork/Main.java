package LinkedListHomeWork;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> l = new MyLinkedList<>();
        l.add("A");
        l.add("B");
        l.add("C");
        for (String s : l) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");
        System.out.println(l.contains("Z"));
        l.remove("B");
        for (String s : l) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");
        System.out.println(l.indexOf("A"));
        l.add("D");
        l.add("A");
        System.out.println(l.lastIndexOf("A"));
        l.add(2, "X");
        for (String s : l) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");

        List<String> l2 = new MyLinkedList<>();
        l2.add("2A");
        l2.add("2B");
        l.addAll(l2);
        for (String s : l) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");

        l.add(3, "H");
        for (String s : l) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");

        l.addAll(3, l2);
        for (String s : l) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");
        l.clear();
        for (String s : l) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");

    }
}
