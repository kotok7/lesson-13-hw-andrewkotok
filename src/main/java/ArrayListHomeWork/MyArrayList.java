package ArrayListHomeWork;

import java.util.*;

public class MyArrayList<T> implements List<T> {
    private static final int INITIAL_SIZE = 8;
    private static final int SCALE_FACTOR = 2;
    private int size = 0;
    private Object[] arr = new Object[INITIAL_SIZE];


    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        validateIndex(fromIndex);
        validateIndex(toIndex);
        MyArrayList<T> tmp = new MyArrayList<>();
        for (int i = fromIndex; i < toIndex; i++) {
            tmp.add((T) arr[i]);
        }
        return tmp;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        MyArrayList<T> list = new MyArrayList<>();
        for (Object elem : c) {
            if (contains(elem)) {
                list.add((T) elem);
                size++;
            }
        }
        arr = Arrays.copyOf(list.toArray(), size);
        return true;
    }

    @Override
    public boolean add(T elem) {
        if (arr.length == size) expand();
        arr[size++] = elem;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T elem : c) {
            add(elem);
        }
        return true;
    }

    @Override
    public void add(int index, T element) {
        validateIndex(index);
        add(element);
        for (int i = size - 1; i > index; i--) {
            arr[i] = arr[i - 1];
        }
        arr[index] = element;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        validateIndex(index);
        Object[] collection = c.toArray();
        if (arr.length == 0 || c.size() == 0) return false;
        Object[] tmp = new Object[arr.length + c.size()];
        for (int i = 0; i < tmp.length; i++) {
            if (i < index) {
                tmp[i] = arr[i];
            } else if (i == index) {
                System.arraycopy(collection, 0, tmp, index, collection.length);
                size = size + collection.length;
            } else {
                System.arraycopy(arr, index, tmp, index + collection.length, arr.length - index);
            }
        }
        arr = Arrays.copyOf(tmp, size);
        return true;
    }

//    add finish

    @Override
    public T remove(int index) {
        validateIndex(index);
        Object[] tmp = new Object[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i == index) {
                size--;
                continue;
            }
            tmp[j++] = arr[i];
        }
        return (T) (arr = Arrays.copyOf(tmp, size));
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) return false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == o) {
                remove(i);
                break;
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (c == null) return false;
        Object[] collection = c.toArray();
        for (Object o : arr) {
            for (Object value : collection) {
                if (o == value) {
                    remove(o);
                }
            }
        }
        return true;
    }

//    remove finish

    @Override
    public int indexOf(Object o) {
        int k = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(o)) {
                k = i;
                break;
            }
        }
        return k;
    }

    @Override
    public int lastIndexOf(Object o) {
        int k = -1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i].equals(o)) {
                k = i;
                break;
            }
        }
        return k;
    }

    @Override
    public boolean contains(Object o) {
        for (Object obj : arr) {
            if (obj.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (c.size() > arr.length) return false;
        if (c.size() == 0 && arr.length == 0) return false;
        for (Object elem : c) {
            if (!contains(elem)) return false;
        }
        return true;
    }

    @Override
    public void clear() {
        size = 0;
        arr = new Object[INITIAL_SIZE];
    }

    @Override
    public ListIterator<T> listIterator() {
        // Do not need to do in this HW.
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        // Do not need to do in this HW.
        return null;
    }

    @Override
    public T get(int index) {
        validateIndex(index);
        return (T) arr[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    @Override
    public T set(int index, T value) {
        validateIndex(index);
        arr[index] = value;
        return value;
    }


    @Override
    public Iterator<T> iterator() {
        return new MyArrayListIterator();
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(arr, size);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] arrCopy = Arrays.copyOf(a, size);
        System.arraycopy(arr, 0, arrCopy, 0, size);
        return arrCopy;
    }

    private void expand() {
        int newLength = arr.length * SCALE_FACTOR;
        arr = Arrays.copyOf(arr, newLength);
    }

    private void validateIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
    }


    private class MyArrayListIterator implements Iterator<T> {
        int i = 0;

        @Override
        public boolean hasNext() {
            return i < size;
        }

        @Override
        public T next() {
            T elem = (T) arr[i];
            i++;
            return elem;
        }
    }
}
