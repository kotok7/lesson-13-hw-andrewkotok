package ArrayListHomeWork;


public class Main {
    public static void main(String[] args) {
        MyArrayList<String> list = new MyArrayList<>();
        MyArrayList<String> list2 = new MyArrayList<>();
        MyArrayList<String> list3 = new MyArrayList<>();
        MyArrayList<String> list4 = new MyArrayList<>();
        System.out.println("boolean add(T elem); boolean addAll(Collection<? extends T> c); \n" +
                "void add(int index, T element); public boolean addAll(int index, Collection<? extends T> c): ");

        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("I");
        list2.add("2A");
        list2.add("2B");
        list2.add("2C");
        list.addAll(list2);
        list.add(3,"X");
        list.addAll(3,list2);
        for (String s : list) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");


        System.out.println("T remove(int index), boolean remove(Object o), boolean removeAll(Collection<?> c): ");
        list.remove(3);
        list.remove("B");
        list.removeAll(list2);
        for (String s : list) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");

        System.out.println("boolean contains(Object o),boolean containsAll(Collection<?> c), int indexOf(Object o), int lastIndexOf(Object o): ");
        System.out.println(list.contains("P"));
        list3.add("A");
        list3.add("D");
        System.out.println(list.containsAll(list3));
        System.out.println(list.indexOf("X"));
        System.out.println(list.lastIndexOf("I"));



        System.out.println("boolean retainAll(Collection<?> c) :");
        list4.add("A");
        list4.add("X");
        list4.add("F");
        list.retainAll(list4);
        for (String s : list) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");
    }
}
